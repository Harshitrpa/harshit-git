from docxcompose.composer import Composer
from docx import Document as Document_compose
from docx import Document
import re

document = Document('combined.docx')
tables = document.tables

class Merging():
    def __init__(self):
        pass
        # master = Document_compose('1_Body Part-1-Pune.docx')
        # composer = Composer(master)
        # doc2 = Document_compose('2_Body Part-2-ABP.docx')
        # #append the doc2 into the master using composer.append function
        # composer.append(doc2)
        # #Save the combined docx with a name
        # composer.save("combined.docx")

    def Replace_and_Save(self, Text_to_find_and_replace, Value_to_itr, del_count,Total_count):
        try:
            count = 1
            if Text_to_find_and_replace in Value_to_itr.text:
                # print(count, "==", Total_count)
                # pass
                # print('SEARCH FOUND!!')
                text = Value_to_itr.text.replace(Text_to_find_and_replace, 'new text')
                # print('HEre**',Text_to_find_and_replace)
                style = Value_to_itr.style
                Value_to_itr.text = text
                Value_to_itr.style = style
                if del_count == Total_count:
                    print(text)
                    # removing_del1=text.replace('[[',"")
                    # removed_del=removing_del1.replace(']]',"")
                    # print(removed_del)
        except Exception as e:
            print(e)

    def Data_to_pass(self):
        Data = {'JOB_REQUISITION_CUSTOM33 ': 'xyz', 'CANDIDATE_FIRST_NAME': 'Harshit',
                'CANDIDATE_LAST_NAME': 'Saini', 'JOB_OFFER_DETAIL_CUSTOM41': 'Det',
                'JOB_REQUISITION_CUSTOM32': '12', 'JOB_OFFER_DETAIL_CUSTOM40': 'xyz1',
                'JOB_OFFER_DETAIL_CUSTOM2': 'ABC', 'JOB_APPLICATION_CUSTOM6': '125',
                'JOB_OFFER_DETAIL_CUSTOM14': '10', 'JOB_APPLICATION_CUSTOM9': 'Unknown',
                'JOB_APPLICATION_CUSTOM3': '88'}

    def Filter_Delimiter(self, Text_value, Param):
        try:
            if "[[" in Text_value.text:
                storetext = Text_value.text
                obj1 = Merging()
                for del_count in range(storetext.count("[[")):
                    Total_count = storetext.count("[[")
                    del_count += 1
                    del_data = Text_value.text.split('[[')
                    del2 = del_data[del_count].split(']]')
                    Text_to_Change = del2[0].strip()
                    obj1.Replace_and_Save(Text_to_Change, Param, del_count,Total_count)
            # else:
            #     print(Text_value.text)

        except Exception as e:
            print(e)

    def doc_text(self):
        try:
            for text_to_read in document.paragraphs:
                obj1 = Merging()
                obj1.Filter_Delimiter(text_to_read, text_to_read)

        except Exception as e:
            print(e)

    def Doc_table_data(self):
        try:
            for table in tables:
                for row in table.rows:
                    for cell in row.cells:
                        for paragraph in cell.paragraphs:
                            obj1 = Merging()
                            obj1.Filter_Delimiter(paragraph,paragraph)
        except Exception as e:
            print(e)


obj1 = Merging()
obj1.doc_text()
obj1.Doc_table_data()
document.save('Completed.docx')
